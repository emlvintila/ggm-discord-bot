﻿using System;
using System.ComponentModel;
using DiscordBotFramework.Model;
using GGM_Discord_Bot.Wow.Api;

namespace GGM_Discord_Bot
{
  [Serializable]
  public class GGMBotConfig : IBotConfig
  {
    public string GuildName { get; set; } = "";

    public string GuildRealm { get; set; } = "";

    public Region Region { get; set; } = Region.US;

    [Description("Guild master's discord role name")]
    public string Rank0Role { get; set; } = "";

    public string Rank1Role { get; set; } = "";
    public string Rank2Role { get; set; } = "";
    public string Rank3Role { get; set; } = "";
    public string Rank4Role { get; set; } = "";
    public string Rank5Role { get; set; } = "";
    public string Rank6Role { get; set; } = "";
    public string Rank7Role { get; set; } = "";
    public string Rank8Role { get; set; } = "";
    public string Rank9Role { get; set; } = "";

    [Description("Additional discord roles to give to all guild members")]
    public string AdditionalRoles { get; set; } = "";

    public char CommandPrefix { get; set; } = '!';
  }
}
