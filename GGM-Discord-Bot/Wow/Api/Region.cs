﻿using System.Diagnostics.CodeAnalysis;

namespace GGM_Discord_Bot.Wow.Api
{
  [SuppressMessage("ReSharper", "InconsistentNaming")]
  public enum Region
  {
    US,
    EU,
    KR,
    TW,
    CN
  }
}
