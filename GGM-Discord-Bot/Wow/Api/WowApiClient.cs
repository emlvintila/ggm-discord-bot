﻿using System;
using System.Net.Http;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using GGM_Discord_Bot.Wow.Api.Data;
using Newtonsoft.Json;

namespace GGM_Discord_Bot.Wow.Api
{
  public class WowApiClient
  {
    // ReSharper disable NotAccessedField.Local
    private readonly string clientId;
    private readonly string clientSecret;
    // ReSharper restore NotAccessedField.Local
    private readonly HttpClient http;
    private readonly Region region;
    private readonly HttpClient tokenClient;
    private Token? token;

    public WowApiClient(string clientId, string clientSecret, Region region)
    {
      this.clientId = clientId;
      this.clientSecret = clientSecret;
      this.region = region;

      tokenClient = new HttpClient
      {
        BaseAddress = new Uri(region switch
        {
          Region.US => "https://us.battle.net",
          Region.EU => "https://eu.battle.net",
          Region.KR => "https://kr.battle.net",
          Region.TW => "https://tw.battle.net",
          Region.CN => "https://battlenet.com.cn",
          _         => throw new ArgumentOutOfRangeException(nameof(region), region, null)
        }, UriKind.Absolute)
      };
      tokenClient.DefaultRequestHeaders.Add("Authorization",
        $"Basic {Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{clientId}:{clientSecret}"))}");

      http = new HttpClient
      {
        BaseAddress =
          new Uri(region switch
          {
            Region.US => "https://us.api.blizzard.com",
            Region.EU => "https://eu.api.blizzard.com",
            Region.KR => "https://kr.api.blizzard.com",
            Region.TW => "https://tw.api.blizzard.com",
            Region.CN => "https://gateway.battlenet.com.cn",
            _         => throw new ArgumentOutOfRangeException(nameof(region), region, null)
          }, UriKind.Absolute)
      };
    }

    public async Task<GuildRoster> GetGuildRoster(string realm, string guild)
    {
      await EnsureOauthTokenValid();
      string json = await http.GetStringAsync(BuildUri("/data/wow/guild/{0}/{1}/roster", realm.Slugify(), guild.Slugify()));
      return JsonConvert.DeserializeObject<GuildRoster>(json);
    }

    private async Task<Token> GetOauthToken()
    {
      const string TokenUrl = "oauth/token";
      using MultipartFormDataContent form = new() {{new StringContent("client_credentials"), "grant_type"}};

      using HttpResponseMessage tokenResponse = await tokenClient.PostAsync(new Uri(BuildUri(TokenUrl), UriKind.Relative), form);
      string tokenJson = await tokenResponse.EnsureSuccessStatusCode().Content.ReadAsStringAsync();

      TokenObject tokenObject = JsonConvert.DeserializeObject<TokenObject>(tokenJson)!;

      return new Token(tokenObject);
    }

    private async Task EnsureOauthTokenValid()
    {
      // if the token expires in less than 5 minutes
      if (token is null || token.ExpiresAt.Subtract(DateTime.Now).TotalSeconds < 300)
        token = await GetOauthToken();

      string accessToken = token.TokenObject.AccessToken;
      http.DefaultRequestHeaders.Remove("Authorization");
      http.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessToken}");
    }

    private string BuildUri(string uri, params object[] args) => string.Format(uri + $"?namespace={GetNamespace()}&locale={GetLocale()}", args).TrimStart('/');

    private string GetNamespace()
    {
      return region switch
      {
        Region.US => "profile-us",
        Region.EU => "profile-eu",
        Region.KR => "profile-kr",
        Region.TW => "profile-tw",
        // Region.Cn => "profile-cn",
        _ => throw new ArgumentOutOfRangeException()
      };
    }

    private string GetLocale()
    {
      return region switch
      {
        Region.US => "en_US",
        Region.EU => "en_GB",
        Region.KR => "ko_KR",
        Region.TW => "zh_TW",
        Region.CN => "zh_CN",
        _         => throw new ArgumentOutOfRangeException()
      };
    }

    [Serializable]
    private class TokenObject
    {
      [JsonPropertyName("access_token")]
      [JsonProperty(PropertyName = "access_token")]
      public string AccessToken { get; set; } = null!;

      [JsonPropertyName("token_type")]
      [JsonProperty(PropertyName = "token_type")]
      public string TokenType { get; set; } = null!;

      [JsonPropertyName("expires_in")]
      [JsonProperty(PropertyName = "expires_in")]
      public int ExpiresIn { get; set; }

      [JsonPropertyName("scope")]
      [JsonProperty(PropertyName = "scope")]
      public string Scope { get; set; } = null!;
    }

    private class Token
    {
      public Token(TokenObject tokenObject)
      {
        TokenObject = tokenObject;
        ExpiresAt = DateTime.Now.AddSeconds(tokenObject.ExpiresIn);
      }

      public TokenObject TokenObject { get; }
      public DateTime ExpiresAt { get; }
    }
  }
}
