﻿using System;
using System.Text.Json.Serialization;
using LiteDB;
using Newtonsoft.Json;

namespace GGM_Discord_Bot.Wow.Api.Data
{
  [Serializable]
  public class GuildRoster
  {
    [System.Text.Json.Serialization.JsonIgnore]
    [Newtonsoft.Json.JsonIgnore]
    [BsonId]
    public long Id { get; set; }

    [JsonPropertyName("members")]
    [JsonProperty(PropertyName = "members")]
    public GuildRosterMember[] Members { get; set; } = null!;

    [JsonPropertyName("guild")]
    [JsonProperty(PropertyName = "guild")]
    public GuildRosterGuild Guild { get; set; } = null!;
  }

  [Serializable]
  public class GuildRosterGuild
  {
    [JsonPropertyName("id")]
    [JsonProperty(PropertyName = "id")]
    public long Id { get; set; }

    [JsonPropertyName("name")]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; } = null!;

    [JsonPropertyName("realm")]
    [JsonProperty(PropertyName = "realm")]
    public GuildRosterGuildRealm Realm { get; set; } = null!;
  }

  public class GuildRosterGuildRealm
  {
    [JsonPropertyName("id")]
    [JsonProperty(PropertyName = "id")]
    public long Id { set; get; }

    [JsonPropertyName("name")]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; } = null!;
  }

  [Serializable]
  public class GuildRosterMember
  {
    [JsonPropertyName("character")]
    [JsonProperty(PropertyName = "character")]
    public GuildRosterMemberCharacter Character { get; set; } = null!;

    [JsonPropertyName("rank")]
    [JsonProperty(PropertyName = "rank")]
    public long Rank { get; set; }
  }

  [Serializable]
  public class GuildRosterMemberCharacter
  {
    [JsonPropertyName("id")]
    [JsonProperty(PropertyName = "id")]
    public long Id { get; set; }

    [JsonPropertyName("name")]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; } = null!;

    [JsonPropertyName("realm")]
    [JsonProperty(PropertyName = "realm")]
    public GuildRosterMemberCharacterRealm Realm { get; set; } = null!;
  }

  [Serializable]
  public class GuildRosterMemberCharacterRealm
  {
    [JsonPropertyName("id")]
    [JsonProperty(PropertyName = "id")]
    public long Id { get; set; }

    [JsonPropertyName("slug")]
    [JsonProperty(PropertyName = "slug")]
    public string Slug { get; set; } = null!;
  }
}
