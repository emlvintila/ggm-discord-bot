﻿using System.Threading.Tasks;
using GGM_Discord_Bot.Wow.Api;
using GGM_Discord_Bot.Wow.Api.Data;
using LiteDB;

namespace GGM_Discord_Bot.Wow.Services
{
  public class WowGuildRosterRepository
  {
    private readonly LiteDatabase db;
    private readonly RegionCache<WowApiClient> wowApiClientsCache;

    public WowGuildRosterRepository(LiteDatabase db, RegionCache<WowApiClient> wowApiClientsCache)
    {
      this.db = db;
      this.wowApiClientsCache = wowApiClientsCache;
    }

    public GuildRoster? GetRoster(string realm, string guild)
    {
      return db.GetCollection<GuildRoster>().FindOne(r => r.Guild.Name == guild && r.Guild.Realm.Name == realm);
    }

    public async Task<GuildRoster> UpdateRoster(string realm, string guild, Region region)
    {
      WowApiClient apiClient = wowApiClientsCache.Get(region);
      GuildRoster roster = await apiClient.GetGuildRoster(realm, guild);

      GuildRoster? localRoster = GetRoster(realm, guild);
      if (localRoster is not null)
        db.GetCollection<GuildRoster>().Update(localRoster.Id, roster);
      else
        db.GetCollection<GuildRoster>().Insert(roster);

      return roster;
    }
  }
}
