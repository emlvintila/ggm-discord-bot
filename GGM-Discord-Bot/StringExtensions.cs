﻿using System.Text.RegularExpressions;

namespace GGM_Discord_Bot
{
  public static class StringExtensions
  {
    public static string Slugify(this string str) =>
      Regex.Replace(str, "[^\\w\\s]*", string.Empty)
        .Replace(' ', '-')
        .ToLowerInvariant();
  }
}
