﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using GGM_Discord_Bot.Wow.Api;

namespace GGM_Discord_Bot
{
  public class RegionCache<TValue>
  {
    private readonly IDictionary<Region, TValue> cache = new ConcurrentDictionary<Region, TValue>();
    private readonly Func<Region, TValue> factory;

    public RegionCache(Func<Region, TValue> factory) => this.factory = factory;

    public TValue Get(Region region)
    {
      if (!cache.ContainsKey(region))
        cache[region] = factory(region);

      return cache[region];
    }
  }
}
