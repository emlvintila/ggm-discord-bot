﻿using System.Collections.Generic;
using System.Linq;

namespace GGM_Discord_Bot
{
  public static class EnumerableExtensions
  {
    public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T?> source)
    {
      return source
        .Where(item => item is not null)
        // ReSharper disable once RedundantEnumerableCastCall
        .Cast<T>();
    }

    public static void Deconstruct<T>(this IEnumerable<T> source, out T? head, out IEnumerable<T> tail)
    {
      // ReSharper disable PossibleMultipleEnumeration
      head = source.FirstOrDefault();
      tail = source.Skip(1);
      // ReSharper restore PossibleMultipleEnumeration
    }
  }
}
