﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using DiscordBotFramework;
using DiscordBotFramework.Data;
using DiscordBotFramework.Model.DAO;
using DiscordBotFramework.Utils;
using GGM_Discord_Bot.Wow.Api;
using GGM_Discord_Bot.Wow.Api.Data;
using GGM_Discord_Bot.Wow.Services;
using LiteDB;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;

namespace GGM_Discord_Bot
{
  internal static class Program
  {
    private static DiscordBot bot = null!;
    private static string wowApiClientId = string.Empty;
    private static string wowApiClientSecret = string.Empty;

    private static readonly RegionCache<WowApiClient> WowApiClientsCache = new(region => new WowApiClient(wowApiClientId, wowApiClientSecret, region));

    // ReSharper disable once NotAccessedField.Local
    private static Timer? guildRosterUpdateTimer;
    private static ILogger logger = null!;

    private static async Task Main()
    {
      string discordBotToken =
        Environment.GetEnvironmentVariable("GGM_DISCORD_BOT_TOKEN") ??
        throw new Exception("No token specified. Specify a discord bot token through the GGM_DISCORD_BOT_TOKEN environment variable.");

      wowApiClientId =
        Environment.GetEnvironmentVariable("GGM_CLIENT_ID")
        ?? throw new Exception("No client ID specified. Specify a Wow API client ID through the GGM_CLIENT_ID environment variable.");
      wowApiClientSecret =
        Environment.GetEnvironmentVariable("GGM_CLIENT_SECRET")
        ?? throw new Exception("No client secret specified. Specify a Wow API client ID through the GGM_CLIENT_SECRET environment variable.");

      ILoggerFactory loggerFactory = LoggerFactory.Create(builder =>
      {
        builder.AddConfiguration();
        builder.AddConsole();
      });
      logger = loggerFactory.CreateLogger(nameof(GGM_Discord_Bot));

      bot = new DiscordBot(discordBotToken)
        .ConfigureServiceCollection(services =>
        {
          services.AddSingleton<WowGuildRosterRepository>();
          services.AddSingleton(WowApiClientsCache);
        })
        .WithConfig<GGMBotConfig>();
      bot.Client.GuildMemberUpdated += ClientOnGuildMemberUpdated;
      bot.Client.Ready += () =>
      {
        guildRosterUpdateTimer = new Timer(_ => UpdateAllGuildRosters().Wait(), null, TimeSpan.Zero, TimeSpan.FromHours(4));
        return Task.CompletedTask;
      };
      bot.Client.Log += message =>
      {
        switch (message.Severity)
        {
          case <= LogSeverity.Error:
            logger.LogError(message.Exception, "{Message}", message.Message);
            break;
          case LogSeverity.Warning:
            logger.LogWarning(message.Exception, "{Message}", message.Message);
            break;
          case LogSeverity.Info:
            logger.LogInformation(message.Exception, "{Message}", message.Message);
            break;
          case >= LogSeverity.Verbose:
            logger.LogDebug(message.Exception, "{Message}", message.Message);
            break;
        }

        return Task.CompletedTask;
      };

      await bot.StartAsync();
      await Task.Delay(-1);
    }

    private static async Task UpdateAllGuildRosters()
    {
      IEnumerable<GGMBotConfig> configs = bot.ServiceProvider!.GetService<LiteDatabase>()!
        .GetCollection<ConfigDao>()
        .FindAll()
        .Select(config => (GGMBotConfig) config.Config)
        .ToImmutableArray();

      logger.LogInformation("Updating rosters for {ConfigCount:D} guilds", configs.Count());
      foreach (GGMBotConfig config in configs)
      {
        Region region = config.Region;
        string name = config.GuildName;
        string realm = config.GuildRealm;
        logger.LogInformation("Updating roster for {Region} {Name}-{Realm}", region, name, realm);
        try
        {
          await bot.ServiceProvider!.GetService<WowGuildRosterRepository>()!
            .UpdateRoster(realm, name, region);
          logger.LogInformation("Successfully updated roster for {Region} {Name}-{Realm}", region, name, realm);
        }
        catch (HttpRequestException e)
        {
          logger.LogWarning(e, "Update failed for {Region} {Name}-{Realm}", region, name, realm);
          // configured guild doesn't exist
        }
      }

      logger.LogInformation("Updating Discord members roles");
      foreach (SocketGuild guild in bot.Client.Guilds)
      {
        await guild.DownloadUsersAsync();
        logger.LogInformation("Updating roles for Discord guild {GuildName} ({GuildId}) ", guild.Name, guild.Id);
        // Don't use Task.WhenAll because we will hit the discord API rate limit
        foreach (SocketGuildUser user in guild.Users)
          await UpdateUserRoles(user);
      }
    }

    private static async Task ClientOnGuildMemberUpdated(SocketGuildUser before, SocketGuildUser user)
    {
      // only handle nickname changes
      if (before.Nickname == user.Nickname)
        return;

      await UpdateUserRoles(user).ConfigureAwait(false);
    }

    private static async Task UpdateUserRoles(SocketGuildUser user)
    {
      bool hasNickname = !string.IsNullOrWhiteSpace(user.Nickname);
      string characterFullName = hasNickname ? user.Nickname : user.Username;
      string[] characterNameAndRealm = characterFullName.Split('-', 2, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
      ImmutableArray<string> characterNameWords = Regex.Split(characterNameAndRealm.First(), @"\b")
        .Where(w => !string.IsNullOrWhiteSpace(w))
        .Select(w => w.Trim())
        .ToImmutableArray();
      string? characterRealmName = characterNameAndRealm.Skip(1).FirstOrDefault();
      string? characterRealmSlug = characterRealmName?.Slugify();

      SocketGuild discordGuild = user.Guild;
      ulong discordGuildId = discordGuild.Id;
      GGMBotConfig config = (GGMBotConfig) bot.ServiceProvider!.GetService<ConfigRepository>()!.GetForGuild(discordGuildId).Config;

      // TODO: Use config.Region in repository
      string wowGuildName = config.GuildName;
      string wowGuildRealm = config.GuildRealm;
      WowGuildRosterRepository wowGuildRosterRepository = bot.ServiceProvider!.GetService<WowGuildRosterRepository>()!;
      GuildRoster? roster = wowGuildRosterRepository.GetRoster(wowGuildRealm, wowGuildName);
      if (roster is null)
        // guild doesn't exist or roster hasn't been initialized yet
        return;

      bool MemberPredicate(GuildRosterMember m) =>
        characterNameWords.Any(characterName => string.Equals(m.Character.Name, characterName, StringComparison.OrdinalIgnoreCase)) &&
        (
          string.IsNullOrWhiteSpace(characterRealmSlug) ||
          string.Equals(m.Character.Realm.Slug, characterRealmSlug, StringComparison.OrdinalIgnoreCase)
        );

      IEnumerable<SocketRole> GetAdditionalRoles() => discordGuild.Roles.Where(role => config.AdditionalRoles.Split(',').Contains(role.Name));

      IEnumerable<SocketRole> GetAllConfiguredRoles() =>
        Enumerable.Range(0, 10)
          .Select(i => $"Rank{i}Role")
          .Select(prop => (string) ReflectionUtils.GetPropertyValue(config, prop)!)
          .Select(roleName => discordGuild.Roles.FirstOrDefault(role => role.Name == roleName))
          .WhereNotNull()
          .Concat(GetAdditionalRoles());

      IEnumerable<SocketRole> allConfiguredRoles = GetAllConfiguredRoles().ToImmutableArray();

      GuildRosterMember? member = roster.Members.Where(MemberPredicate).OrderBy(m => m.Rank).FirstOrDefault();
      // remove roles if member changes nickname
      if (member is null)
      {
        // only remove if bot has higher role than member
        int botHighestRole = discordGuild.GetUser(bot.Client.CurrentUser.Id).Roles.Max(role => role.Position);
        int memberHighestRole = user.Roles.Max(role => role.Position);
        if (botHighestRole <= memberHighestRole)
          return;

        logger.LogInformation("Removing roles for {Username}#{Discriminator} {Nickname}",
          user.Username, user.Discriminator, hasNickname ? $"({user.Nickname})" : "");
        await user.RemoveRolesAsync(allConfiguredRoles);
      }
      // grant roles to member
      else
      {
        // handle wow guild promotions by removing the old role
        IEnumerable<SocketRole> rolesToBeAdded = GetAdditionalRoles();

        string discordRoleString = (string) ReflectionUtils.GetPropertyValue(config, $"Rank{member.Rank}Role")!;

        SocketRole? discordRole = discordGuild.Roles.FirstOrDefault(role => role.Name == discordRoleString);
        if (discordRole is not null)
          rolesToBeAdded = rolesToBeAdded.Concat(new[] {discordRole});

        rolesToBeAdded = rolesToBeAdded.ToImmutableArray();
        IEnumerable<SocketRole> rolesToBeRemoved = user.Roles.Intersect(allConfiguredRoles).Except(rolesToBeAdded);

        logger.LogInformation("Updating roles for {Username}#{Discriminator} {Nickname} = {CharacterName}-{RealmName}",
          user.Username, user.Discriminator, hasNickname ? $"({user.Nickname}) " : "",
          member.Character.Name, member.Character.Realm.Slug);
        await user.AddRolesAsync(rolesToBeAdded);
        await user.RemoveRolesAsync(rolesToBeRemoved);
      }
    }
  }
}
