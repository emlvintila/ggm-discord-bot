FROM mcr.microsoft.com/dotnet/sdk:5.0-alpine AS build

COPY . /build
WORKDIR /build
RUN ["dotnet", "publish", "-r", "linux-musl-x64", "-c", "Release", "-o", "/publish", "--self-contained", "true"]

FROM mcr.microsoft.com/dotnet/runtime:5.0-alpine AS runtime
COPY --from=build /publish /app
WORKDIR /app
ENTRYPOINT ["./GGM-Discord-Bot"]
